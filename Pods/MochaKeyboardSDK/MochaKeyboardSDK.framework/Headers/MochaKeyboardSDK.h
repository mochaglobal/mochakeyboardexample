//
//  MochaKeyboardSDK.h
//  MochaKeyboardSDK
//
//  Created by Vojtech Vrbka on 17/06/2021.
//  Copyright © 2021 Mihai. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MochaKeyboardSDK.
FOUNDATION_EXPORT double MochaKeyboardSDKVersionNumber;

//! Project version string for MochaKeyboardSDK.
FOUNDATION_EXPORT const unsigned char MochaKeyboardSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MochaKeyboardSDK/PublicHeader.h>

