//
//  MochaSDK.h
//  MochaSDK
//
//  Created by Vojtech Vrbka on 05/08/2020.
//  Copyright © 2020 Mocha. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MochaSDK.
FOUNDATION_EXPORT double MochaSDKVersionNumber;

//! Project version string for MochaSDK.
FOUNDATION_EXPORT const unsigned char MochaSDKVersionString[];

#include "api.h"
#include "libstemmer.h"
#include "modules_utf8.h"
#include "sqlite_functions.h"
