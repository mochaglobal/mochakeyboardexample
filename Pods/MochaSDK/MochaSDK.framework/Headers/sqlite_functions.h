#include <sqlite3.h>
#include "libstemmer.h"
#include <string.h>
#include "sqlite3ext.h"


int sqlite3_snowball_init(sqlite3 *db);
int sqlite3_spellfix_init(sqlite3 *db);
