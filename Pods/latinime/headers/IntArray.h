// ©2019 Mihai Cristian Tanase. All rights reserved.

/**
 * IntArray is a basic wrapper over an array of integer numbers.
 * The data is copied in a new memory zone which is released when the ObjC
 * object is deallocated.
 * This means that whoever created "data" is also responsable for releasing
 * it's memory.
 *
 * OBS(mihai): this is way better than using the classical NSArray or
 * NSMutableArray data structures because it does not require wrapping every
 * single element in an NSNumber.
 */

#import <Foundation/Foundation.h>

@interface IntArray : NSObject

@property (assign, readonly, nonatomic) int* data;
@property (assign, nonatomic) int count;

- (instancetype)initWithData:(int*)data count:(int)count;
- (instancetype)initWithData:(NSArray<NSNumber*>*)data;
+ (instancetype)withData:(int*)data count:(int)count;
+ (instancetype)withData:(NSArray<NSNumber*>*)data;
+ (instancetype)withRepeatedValue:(int)value count:(int)count;

@end
