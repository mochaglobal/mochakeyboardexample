// ©2019 Mihai Cristian Tanase. All rights reserved.

#import "IntArray.h"
#import "ProximityInfo.h"
#import "constants.h"
#import <Foundation/Foundation.h>

@interface CSP : NSObject

/**
 * Load the binary dictioanry with the given name.
 */
- (BOOL)loadDictionaryNamed:(NSString*)name userShortcutsPath:(NSString*)userShortcutsPath;

/**
 * Load the proximity info.
 */
- (void)loadProximityInfoWithKbdWidth:(int)kbdWidth
                         andKbdHeight:(int)kbdHeight
                              andKeys:(NSArray<LIMEKey*>*)keys;

/**
 * Correction.
 */
- (NSString*)correctionForWord:(NSString*)wordToCorrect
                  xCoordinates:(IntArray*)xCoordinates
                  yCoordinates:(IntArray*)yCoordinates;

/**
 * Suggestion.
 */
- (NSArray<NSString*>*)suggestionsForPartialWord:(NSString*)partialWord
                                    xCoordinates:(IntArray*)xCoordinates
                                    yCoordinates:(IntArray*)yCoordinates;

/**
 * Prediction.
 */
- (NSArray<NSString*>*)predictionsForWords:(NSArray<NSString*>*)prevWords;

/**
 * Gesture.
 */
- (NSArray<NSString*>*)suggestionsForGestureWithWords:(NSArray<NSString*>*)prevWords
                                         xCoordinates:(IntArray*)xCoordinates
                                         yCoordinates:(IntArray*)yCoordinates
                                           timestamps:(IntArray*)timestamps
                                isBeginningOfSentence:(BOOL)isBeginningOfSentence;

/**
 * Returns true only if the Correction-Suggestion-Prediction trio is ready
 * to be used.
 */
- (BOOL)valid;

- (NSUInteger)nrWords;

@end
