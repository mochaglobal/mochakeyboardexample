// ©2019 Mihai Cristian Tanase. All rights reserved.

#import <Foundation/Foundation.h>

@interface LIMEKey : NSObject

@property (assign, nonatomic) int codePoint;
@property (assign, nonatomic) double x;
@property (assign, nonatomic) double y;
@property (assign, nonatomic) double w;
@property (assign, nonatomic) double h;

- (instancetype)initWithCodePoint:(int)codePoint x:(double)x y:(double)y w:(double)w h:(double)h;

- (int)squaredDistanceToEdgeOfX:(double)x andY:(double)y;

@end

@interface ProximityInfo : NSObject

- (instancetype)initWithKbdWidth:(int)kbdWidth
                    andKbdHeight:(int)kbdHeight
                         andKeys:(NSArray<LIMEKey*>*)kbdKeys;

- (int64_t)proximityInfoNativePtr;

+ (BOOL)isReleased:(int64_t)ptr;

+ (int)mostCommonKeyWidth:(NSArray<LIMEKey*>*)keys;
+ (int)mostCommonKeyHeight:(NSArray<LIMEKey*>*)keys;

@end
