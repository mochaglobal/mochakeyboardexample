// ©2019 Mihai Cristian Tanase. All rights reserved.

#import "IntArray.h"
#import <Foundation/Foundation.h>

@interface HistoricalInfo : NSObject
@property (assign, nonatomic) int timestamp;
@property (assign, nonatomic) int level;
@property (assign, nonatomic) int count;
@end

@interface ShortcutProperty : NSObject
@property (strong, nonatomic) IntArray* codePoints;
@property (assign, nonatomic) float probability;
@end

@interface UnigramProperty : NSObject
@property (assign, nonatomic) BOOL beginningOfSentence;
@property (assign, nonatomic) BOOL notAWord;
@property (assign, nonatomic) BOOL blacklisted;
@property (assign, nonatomic) BOOL possiblyOffensive;
@property (assign, nonatomic) float probability;
@property (strong, nonatomic) HistoricalInfo* historicalInfo;
@property (strong, nonatomic) NSMutableArray<ShortcutProperty*>* shortcuts;
@end

@interface NgramProperty : NSObject
@property (strong, nonatomic) IntArray* codePoints;
@property (assign, nonatomic) float probability;
@property (strong, nonatomic) HistoricalInfo* historicalInfo;
@end

@interface WordProperty : NSObject

@property (strong, nonatomic) IntArray* codePoints;
@property (strong, nonatomic) UnigramProperty* unigramProperty;
@property (strong, nonatomic) NSMutableArray<NgramProperty*>* ngrams;

- (instancetype)initWithDict:(int64_t)ptr
                  codePoints:(int*)codePoints
             codePointsCount:(int)codePointsCount
       isBeginningOfSentence:(BOOL)isBeginningOfSentence;

@end
