// ©2019 Mihai Cristian Tanase. All rights reserved.

#import "WordProperty.h"
#import <Foundation/Foundation.h>

@interface BinaryDictionary : NSObject

/**
 * Customize the location where the dictionaries folder can be found.
 */
+ (void)setDictionariesPath:(NSString*)path;

/**
 * Create a new instance of BinaryDictionary which loads the dictionary with
 * the give name. The actual file that is loaded is located at a path composed
 * as follows: <bundle_folder>/dictionaries/<name>. So the name has to be the
 * complete file name (including the extension).
 * userShortcutsPath is the full path to the file containing custom user shortcuts.
 */
- (instancetype)initWithName:(NSString*)name userShortcutsPath:(NSString*)userShortcutsPath;

/**
 * Debugging method that prints the entire contents of the loaded dictionary
 * in the system logs.
 */
- (void)logEntireDictionary;

- (int64_t)binaryDictionaryNativePtr;
- (uint64_t)binaryDictionarySize;
- (BOOL)isCorrupted;
- (int)formatVersion;
- (NSDictionary<NSString*, NSObject*>*)headerInfo;
- (NSArray<WordProperty*>*)allWords;

+ (BOOL)isReleased:(int64_t)ptr;

@end
