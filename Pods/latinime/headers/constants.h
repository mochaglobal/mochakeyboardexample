// ©2019 Mihai Cristian Tanase. All rights reserved.

#ifndef LATINIME_CONSTANTS_H
#define LATINIME_CONSTANTS_H

// Must be equal to Constants.Dictionary.MAX_WORD_LENGTH in Java
#define MAX_WORD_LENGTH 48

#endif // LATINIME_CONSTANTS_H
