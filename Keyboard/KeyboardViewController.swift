// ©2021 Mocha. All rights reserved.

import UIKit
import MochaKeyboardSDK

class KeyboardViewController: MochaKeyboardViewController {

    override func configuration() -> (
        config: MochaKeyboardSDKConfig,
        platformSettings: MochaPlatformSettings,
        themesDirectoryURL: URL?
    ) {
        (
            config: .config,
            platformSettings: .settings,
            themesDirectoryURL: Bundle.main.resourceURL?.appendingPathComponent("Themes", isDirectory: true)
        )
    }
}
