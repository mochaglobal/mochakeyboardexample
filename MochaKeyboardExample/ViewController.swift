// ©2021 Mocha. All rights reserved.

import UIKit
import MochaKeyboardSDK

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!

    @IBAction func startOnboardingTapped(_ sender: Any) {
        OnboardingRouter.present(fromViewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        switch MochaKeyboardSDK.Permissions.getState() {
        case .onboardingFinished:
            button.setTitle("Open settings", for: .normal)
        default:
            button.setTitle("Start onboarding", for: .normal)
        }

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        switch MochaKeyboardSDK.Permissions.getState() {
        case .onboardingFinished:
            break
        default:
            OnboardingRouter.present(fromViewController: self)
        }
    }
}
