// ©2021 Mocha. All rights reserved.

import UIKit
import MochaKeyboardSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        MochaKeyboardSDK.initialize(config: .config, platformSettings: .settings)
        return true
    }

    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

        if MochaKeyboardSDK.shouldOpenSettings(url: url) {
            let navigationController = UINavigationController(rootViewController: UIViewController())
            navigationController.isNavigationBarHidden = true

            let settingsVC = MochaKeyboardSDK.SettingsViewController(pushCallback: {navigationController.pushViewController($0, animated: true) }, iconImage: UIImage(named: "ic_title_logo"))
            navigationController.pushViewController(settingsVC, animated: false)

            window?.rootViewController?.present(navigationController, animated: true, completion: {

            })
        }
        return true
    }
}
