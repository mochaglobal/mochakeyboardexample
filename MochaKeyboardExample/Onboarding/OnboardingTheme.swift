// ©2021 Mocha. All rights reserved.

import UIKit

enum OnboardingTheme {
    static let background = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let text1 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let text2 = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
    static let buttonBackground = #colorLiteral(red: 0, green: 0.7137254902, blue: 0.9764705882, alpha: 1)
    static let buttonText = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

    static let privacyDialogTitleTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let privacyDialogMessageTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
}
