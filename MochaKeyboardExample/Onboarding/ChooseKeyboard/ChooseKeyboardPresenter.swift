// ©2021 Mocha. All rights reserved.

import UIKit

final class ChooseKeyboardPresenter: Presenter<ChooseKeyboardViewController> {
    init() {
        let vm = ChooseKeyboardViewModel()
        let vc = ChooseKeyboardViewController.create()
        vc.setViewModel(vm)
        super.init(vc)
    }
}
