// ©2021 Mocha. All rights reserved.

import UIKit
import Lottie

private let kDelay: TimeInterval = 1.0

final class ChooseKeyboardViewController: UIViewController {
    @IBOutlet var tapGlobeL: UILabel!
    @IBOutlet var selectKbdL: UILabel!
    @IBOutlet var checkTF: UITextField!
    @IBOutlet var gifIV: AnimationView!
    @IBOutlet weak var allThingsPrivateL: EdgeInsetLabel!
    @IBOutlet weak var step2L: UILabel!
    @IBOutlet weak var globeIV: UIImageView!
    @IBOutlet weak var checkIV: UIImageView!
    
    //UI constants for device size adjustaments
    @IBOutlet var logoTC: NSLayoutConstraint!
    @IBOutlet var logoHC: NSLayoutConstraint!
    @IBOutlet var gifHC: NSLayoutConstraint!
    @IBOutlet var titleTC: NSLayoutConstraint!
    @IBOutlet var firstLineTC: NSLayoutConstraint!
    @IBOutlet var secondLineTC: NSLayoutConstraint!

    private var vm: ChooseKeyboardViewModel! {
        didSet {
            vm.bindActionCompleted = { [weak self] in self?.goToNextScreenDelayed() }
        }
    }

    func setViewModel(_ vm: ChooseKeyboardViewModel) {
        self.vm = vm
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        logoTC.constant *= scaleForTopDim
        logoHC.constant *= scaleForLogoHeightDim
        gifHC.constant *= scaleForHeightDim
        titleTC.constant *= scaleForTopVerticalDim
        firstLineTC.constant *= scaleForTopVerticalDim
        secondLineTC.constant *= scaleForTopVerticalDim

        tapGlobeL.attributedText = vm.tapGlobeText
        selectKbdL.attributedText = vm.selectKbdText
        step2L.text = vm.step2LText
        
        setupAnimation()
        
        applyColors()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkTF.becomeFirstResponder()
    }

    private func goToNextScreenDelayed() {
        DispatchQueue.main.asyncAfter(deadline: .now() + kDelay) {
            OnboardingRouter.showNextOnboardingScreen()
        }
    }

    // MARK: - UI customization
    
    private func applyColors() {
        view.backgroundColor = vm.backgroundColor
        
        allThingsPrivateL.textColor = vm.allThingsPrivateColor
        step2L.textColor = vm.step2Color
        tapGlobeL.textColor = vm.tapGlobeColor
        selectKbdL.textColor = vm.selectKbdColor
        
        globeIV.tintColor = vm.globeColor
        checkIV.tintColor = vm.checkColor
    }
    
    private func setupAnimation() {
        gifIV.animation = vm.animationResources.0
        if let imageProvider = vm.animationResources.1 {
            gifIV.imageProvider = imageProvider
        }
        gifIV.loopMode = .loop
        gifIV.play()
    }
}
