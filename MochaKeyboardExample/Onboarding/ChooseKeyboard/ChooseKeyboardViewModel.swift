// ©2021 Mocha. All rights reserved.

import UIKit
import Lottie
import ZIPFoundation
import MochaKeyboardSDK

private let kPollRefresh = 0.05
private let animationFolder = Bundle.main.resourceURL!.appendingPathComponent("choose_kbd", isDirectory: true)

final class ChooseKeyboardViewModel {
    let tapGlobeText = attributedBold(
        for: "Tap and hold Globe icon",
        boldText: "Globe",
        size: 15.0
    )
    let selectKbdText = attributedBold(
        for: "Select Keemoji",
        boldText: "Keemoji",
        size: 15.0
    )

    let step2LText = "Step 2: Switch to Keemoji"

    var animationResources: (Animation?, AnimationImageProvider?) {
        var animationDataPath = "data.json"
        if let array = try? FileManager.default.contentsOfDirectory(atPath: animationFolder.relativePath) {
            animationDataPath = array.filter { $0.contains(".json") }.first ?? "data.json"
        }
        return (Animation.filepath(animationFolder.appendingPathComponent(animationDataPath).relativePath),
                FilepathImageProvider(filepath: animationFolder.relativePath))
    }

    var bindActionCompleted: (() -> Void)?

    private var timer: Timer!

    init() {
        timer = Timer.scheduledTimer(
            withTimeInterval: kPollRefresh,
            repeats: true,
            block: { [weak self] _ in
                if self == nil || MochaKeyboardSDK.Permissions.isKeyboardChosen() {
                    self?.timer.invalidate()
                    MochaKeyboardSDK.Permissions.setKeyboardAlreadyChosen(true)
                    self?.bindActionCompleted?()
                }
            }
        )
    }

    // MARK: - UI customization

    var backgroundColor: UIColor {
        return OnboardingTheme.background
    }

    var allThingsPrivateColor: UIColor {
        return OnboardingTheme.text1
    }

    var step2Color: UIColor {
        return OnboardingTheme.text2
    }

    var tapGlobeColor: UIColor {
        return OnboardingTheme.text1
    }

    var selectKbdColor: UIColor {
        return OnboardingTheme.text1
    }

    var globeColor: UIColor {
        return OnboardingTheme.text1
    }

    var checkColor: UIColor {
        return OnboardingTheme.text1
    }
}
