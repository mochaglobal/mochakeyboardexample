// ©2021 Mocha. All rights reserved.

import UIKit

extension UICollectionView {
    func register(_ cellId: String) {
        let nib = UINib(nibName: cellId, bundle: .main)
        register(nib, forCellWithReuseIdentifier: cellId)
    }
}
