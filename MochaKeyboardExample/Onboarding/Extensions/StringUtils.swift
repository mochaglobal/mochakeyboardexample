// ©2021 Mocha. All rights reserved.

import UIKit

public func attributedBold(
    for text: String,
    boldText: String,
    size: CGFloat
) -> NSMutableAttributedString {
    let attr = NSMutableAttributedString(string: text)
    let range = attr.rangeForText(boldText)!
    attr.addAttribute(NSAttributedString.Key.font,
                      value: UIFont.boldSystemFont(ofSize: size),
                      range: range)
    return attr
}
