// ©2021 Mocha. All rights reserved.

import Foundation

extension NSMutableAttributedString {
    func rangeForText(_ text: String) -> NSRange? {
        let foundRange = mutableString.range(of: text)
        if foundRange.location != NSNotFound {
            return foundRange
        }
        return nil
    }
}
