// ©2021 Mocha. All rights reserved.

import UIKit

extension UITableView {
  func register(_ cellId: String) {
    register(
        UINib(nibName: cellId, bundle: .main),
      forCellReuseIdentifier: cellId
    )
  }
}
