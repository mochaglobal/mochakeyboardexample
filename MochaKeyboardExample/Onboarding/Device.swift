// ©2021 Mocha. All rights reserved.

import UIKit

public class Device {
    public static var isPortraitOrientation: Bool {
        let size = UIScreen.main.bounds.size
        return size.width < size.height
    }

    public static var screenSize: CGSize {
        UIScreen.main.bounds.size
    }

    public static var width = { screenSize.width }()
    public static var height = { screenSize.height }()

    static let ipad = UIDevice.current.userInterfaceIdiom == .pad
    static let iphone = UIDevice.current.userInterfaceIdiom == .phone

    public static let maxDimension = max(width, height)
    public static let minDimension = min(height, height)
    public static let iphone4OrLess = iphone && maxDimension < 568.0
    public static let iphone5OrLess = iphone && maxDimension <= 568.0
    public static let iphone5 = iphone && maxDimension == 568.0
    public static let iphone6 = iphone && maxDimension == 667.0
    public static let iphone6p = iphone && maxDimension == 736.0
    public static let iphoneX = iphone && maxDimension == 812.0
    public static let iphoneXI = iphone && maxDimension == 896.0
    public static let iphoneXMax = iphone && maxDimension == 896.0
    public static let iphoneXIPro = iphone && maxDimension == 812.0
    public static let iphoneXIProMax = iphone && maxDimension == 896.0
    public static let iphoneXIIMini = iphone && maxDimension == 896.0
    public static let iphoneXII = iphone && maxDimension == 844.0
    public static let iphoneXIIPro = iphone && maxDimension == 844.0
    public static let iphoneXIIProMaxOrGrater = iphone && maxDimension >= 926.0

    public enum Model: String {
        case simulator = "simulator/sandbox"
        case iPod1 = "iPod 1"
        case iPod2 = "iPod 2"
        case iPod3 = "iPod 3"
        case iPod4 = "iPod 4"
        case iPod5 = "iPod 5"
        case iPad2 = "iPad 2"
        case iPad3 = "iPad 3"
        case iPad4 = "iPad 4"
        case iPhone4 = "iPhone 4"
        case iPhone4S = "iPhone 4S"
        case iPhone5 = "iPhone 5"
        case iPhone5S = "iPhone 5S"
        case iPhone5C = "iPhone 5C"
        case iPadMini1 = "iPad Mini 1"
        case iPadMini2 = "iPad Mini 2"
        case iPadMini3 = "iPad Mini 3"
        case iPadAir1 = "iPad Air 1"
        case iPadAir2 = "iPad Air 2"
        case iPhone6 = "iPhone 6"
        case iPhone6plus = "iPhone 6 Plus"
        case iPhone6S = "iPhone 6S"
        case iPhone6Splus = "iPhone 6S Plus"
        case iPhoneSE = "iPhone SE"
        case iPhone7 = "iPhone 7"
        case iPhone7plus = "iPhone 7 Plus"
        case iPhone8 = "iPhone 8"
        case iPhone8plus = "iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case iPhoneXS = "iPhone XS"
        case iPhoneXSMax = "iPhone XS Max"
        case iPhoneXR = "iPhone XR"
        case iPhoneXI = "iPhone 11"
        case iPhoneXIPro = "iPhone 11 Pro"
        case iPhoneXIProMax = "iPhone 11 Pro Max"
        case iPhoneXIIMini = "iPhone 12 Mini"
        case iPhoneXII = "iPhone 12"
        case iPhoneXIIPro = "iPhone 12 Pro"
        case iPhoneXIIProMax = "iPhone 12 Pro Max"
        case unrecognized
    }
}
