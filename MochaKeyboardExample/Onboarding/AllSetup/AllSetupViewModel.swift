// ©2021 Mocha. All rights reserved.

import UIKit

final class AllSetupViewModel {
    // MARK: - UI customization

    var backgroundColor: UIColor {
        return OnboardingTheme.background
    }

    var allThingsPrivateColor: UIColor {
        return OnboardingTheme.text1
    }

    var allSetUpColor: UIColor {
        return OnboardingTheme.text1
    }

    var buttonBackgroundColor: UIColor {
        return OnboardingTheme.buttonBackground
    }

    var buttonTitleColor: UIColor {
        return OnboardingTheme.buttonText
    }
}
