// ©2021 Mocha. All rights reserved.

import UIKit

final class AllSetupViewController: UIViewController {
    private var vm: AllSetupViewModel!
    @IBOutlet var showKbdTF: UITextField!
    @IBOutlet weak var allThingsPrivateL: EdgeInsetLabel!
    @IBOutlet weak var allSetupL: UILabel!
    @IBOutlet weak var keyboardSettingsB: UIButton!
    
    //UI constants for device size adjustaments
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    
    func setViewModel(_ vm: AllSetupViewModel) {
        self.vm = vm
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        applyColors()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Defaults.onboardingFinished = true
        showKbdTF.becomeFirstResponder()
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            
            let window = UIApplication.shared.windows[0]
            let topPadding = window.safeAreaInsets.top
            let containerHeight = Device.height - keyboardHeight - topPadding - 16
            
            containerHeightConstraint.constant = containerHeight
            
            keyboardSettingsB.layer.cornerRadius = containerHeight * 0.05364806866
        }
    }

    @IBAction func pressedCustomise(_: Any) {
        OnboardingRouter.showNextOnboardingScreen()
    }

    // MARK: - UI customization
    
    private func applyColors() {
        view.backgroundColor = vm.backgroundColor
        
        allThingsPrivateL.textColor = vm.allThingsPrivateColor
        allSetupL.textColor = vm.allSetUpColor

        keyboardSettingsB.backgroundColor = vm.buttonBackgroundColor
        keyboardSettingsB.setTitleColor(vm.buttonTitleColor, for: .normal)
    }
}
