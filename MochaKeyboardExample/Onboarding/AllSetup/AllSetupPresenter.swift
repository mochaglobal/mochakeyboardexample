// ©2021 Mocha. All rights reserved.

import UIKit

final class AllSetupPresenter: Presenter<AllSetupViewController> {
    init() {
        let vm = AllSetupViewModel()
        let vc = AllSetupViewController.create()
        vc.setViewModel(vm)
        super.init(vc)
    }
}
