// ©2021 Mocha. All rights reserved.

import UIKit

let scaleForTopDim = CGFloat(arcase(
    (Device.iphone5OrLess, 0.2),
    (Device.iphone6, 0.3),
    (Device.iphone6p, 0.4),
    (Device.iphoneXIPro, 0.4),
    (true, 1)
    )!)

let scaleForTopVerticalDim = CGFloat(arcase(
    (Device.iphone5OrLess, 0.5),
    (Device.iphone6, 0.65),
    (Device.iphone6p, 0.65),
    (Device.iphoneXIPro, 0.75),
    (true, 1)
    )!)

let scaleForTopVerticalDimPager = CGFloat(arcase(
    (Device.iphone5OrLess, 0.5),
    (Device.iphone6, 0.65),
    (true, 1)
    )!)

let scaleForHeightDim = CGFloat(arcase(
    (Device.iphone5OrLess, 0.5),
    (Device.iphone6, 0.75),
    (Device.iphone6p, 0.9),
    (Device.iphoneXIPro, 0.85),
    (Device.iphoneXIIPro, 0.85),
    (true, 1)
    )!)

let scaleForLogoHeightDim = CGFloat(arcase(
    (Device.iphone5OrLess, 0.5),
    (Device.iphone6, 0.75),
    (Device.iphone6p, 0.9),
    (true, 1)
    )!)


public func arcase<T>(_ cases: (Bool, T)...) -> T! {
  for candidate in cases {
    if candidate.0 {
      return candidate.1
    }
  }
  return nil
}
