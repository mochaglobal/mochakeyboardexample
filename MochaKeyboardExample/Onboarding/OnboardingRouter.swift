// ©2021 Mocha. All rights reserved.

import UIKit
import MochaKeyboardSDK

final class OnboardingRouter {
    private weak static var currentScreen: UIViewController?
    private static var navigationController: UINavigationController?

    static func present(fromViewController: UIViewController) {
        guard let vc = getNextOnboardingScreen() else {
            return
        }
        currentScreen = vc

        navigationController = UINavigationController(rootViewController: vc)
        navigationController!.isNavigationBarHidden = true
        fromViewController.present(navigationController!, animated: true) {
        }
    }

    static func showNextOnboardingScreen() {
        guard let screen = getNextOnboardingScreen() else {
            return
        }
        push(screen)
    }

    static func showPopup(_ popupScreen: UIViewController) {
        popupScreen.modalPresentationStyle = .overCurrentContext
        popupScreen.modalTransitionStyle = .crossDissolve
        currentScreen?.present(popupScreen, animated: true)
    }
    
    static func push(_ screen: UIViewController) {
        navigationController?.pushViewController(screen, animated: true)
        currentScreen = screen
    }

    private static func getNextOnboardingScreen() -> UIViewController? {
        if !Defaults.hasSeenGetStarted {
            return GetStartedPresenter().screen()
        } else {
            switch MochaKeyboardSDK.Permissions.getState() {
            case .none, .keyboardAddded:
                return ActivatePresenter().screen()
            case .fullAccessAllowed:
                return ChooseKeyboardPresenter().screen()
            case .keyboardChosen:
                return AllSetupPresenter().screen()
            case .onboardingFinished:
                return MochaKeyboardSDK.SettingsViewController(pushCallback: push, iconImage: UIImage(named: "ic_title_logo"))
            default:
                return nil
            }
        }
    }
}
