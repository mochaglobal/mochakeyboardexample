// ©2021 Mocha. All rights reserved.

import UIKit

final class GetStartedViewController: UIViewController {
    @IBOutlet var titleL: UILabel!
    @IBOutlet var subtitleL: UILabel!
    @IBOutlet var imagePagerV: ImagePagerView!
    @IBOutlet weak var getStartedB: UIButton!
    
    //UI constants for device size adjustaments
    @IBOutlet var getStartedTC: NSLayoutConstraint!
    @IBOutlet var titleTC: NSLayoutConstraint!

    private var vm: GetStartedViewModel! {
        didSet {
            vm.bindShowPage = { [weak self] in self?.showPage($0) }
            vm.bindCurrentPage = { [weak self] in return self?.getCurrentPage() }
        }
    }

    func setViewModel(_ vm: GetStartedViewModel) {
        self.vm = vm
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        getStartedTC.constant *= scaleForTopVerticalDimPager
        titleTC.constant *= scaleForTopVerticalDimPager

        titleL.text = vm.pageTitle[0]
        subtitleL.text = vm.pageSubtitle[0]

        imagePagerV.picsUrls = vm.pageImages
        imagePagerV.imagesCV.referenceView = view
        imagePagerV.callback = onPageChanged
        
        applyColors()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vm.startAutoPageChange()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        vm.stopAutoPageChange()
    }

    func onPageChanged(page: Int) {
        titleL.text = vm.pageTitle[page]
        subtitleL.text = vm.pageSubtitle[page]
        vm.startAutoPageChange()
    }


    @IBAction func pressedGetStarted(_: Any) {
        Defaults.hasSeenGetStarted = true
        OnboardingRouter.showNextOnboardingScreen()
    }

    // MARK: - UI customization
    
    private func applyColors() {
        view.backgroundColor = vm.backgroundColor
        
        imagePagerV.applyColors(tintColor: vm.imagePagerTintColor, currentPageTintColor: vm.imagePagerCurrentTintColor)
        
        titleL.textColor = vm.titleColor
        subtitleL.textColor = vm.subTitleColor

        getStartedB.backgroundColor = vm.buttonBackgroundColor
        getStartedB.setTitleColor(vm.buttonTitleColor, for: .normal)
    }

    // MARK: - Internal Logic

    private func getCurrentPage() -> Int {
        return imagePagerV.currentPage
    }

    private func showPage(_ page: Int) {
        imagePagerV.showPage(page)
    }
}
