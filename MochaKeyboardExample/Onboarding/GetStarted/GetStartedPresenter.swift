// ©2021 Mocha. All rights reserved.

import UIKit
import Foundation

final class GetStartedPresenter: Presenter<GetStartedViewController> {
    init() {
        let vm = GetStartedViewModel()
        let vc = GetStartedViewController.create()
        vc.setViewModel(vm)

        super.init(vc)


  
    }
}
