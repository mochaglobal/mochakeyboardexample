// ©2021 Mocha. All rights reserved.

import Foundation
import UIKit
import MochaKeyboardSDK

private let kAutoPagerChangeTime = 3.0

final class GetStartedViewModel {
    let pageTitle = ["Social",
                     "100% Privacy",
                     "Fast Swipe Type",
                     "Make it yours",
                     "Make it fun!"]
    let pageSubtitle = ["You never miss a story",
                        "All things private. Period.",
                        "Super fast. Super Human",
                        "Customize. Unlimited possibilities",
                        "Stickers, emojis and much more"]
    let pageImages = ["pic_onb1", "pic_onb2", "pic_onb3", "pic_onb4", "pic_onb5"]

    var bindShowPage: ((Int) -> Void)?
    var bindCurrentPage: (() -> Int?)?
    private var localTimer: Timer?

    func startAutoPageChange() {
        stopAutoPageChange()
        localTimer = Timer.scheduledTimer(
            withTimeInterval: kAutoPagerChangeTime,
            repeats: true,
            block: { [weak self] _ in
                guard let self = self else { return }
                var page = self.bindCurrentPage?() ?? 0
                page = (page + 1) % self.pageImages.count
                self.bindShowPage?(page)
            }
        )
    }

    func stopAutoPageChange() {
        localTimer?.invalidate()
    }
    
    // MARK: - UI customization

    var backgroundColor: UIColor {
        return OnboardingTheme.background
    }

    var imagePagerTintColor: UIColor {
        return OnboardingTheme.text2
    }

    var imagePagerCurrentTintColor: UIColor {
        return OnboardingTheme.text1
    }

    var titleColor: UIColor {
        return OnboardingTheme.text1
    }

    var subTitleColor: UIColor {
        return OnboardingTheme.text2
    }

    var buttonBackgroundColor: UIColor {
        return OnboardingTheme.buttonBackground
    }

    var buttonTitleColor: UIColor {
        return OnboardingTheme.buttonText
    }
}
