
import Foundation

final class Defaults {

    private enum Keys: String, CaseIterable {
        case hasSeenGetStarted
        case onboardingFinished
    }

    static var hasSeenGetStarted: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Keys.hasSeenGetStarted.rawValue)
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: Keys.hasSeenGetStarted.rawValue)
        }
    }

    static var onboardingFinished: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Keys.onboardingFinished.rawValue)
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: Keys.onboardingFinished.rawValue)
        }
    }
}
