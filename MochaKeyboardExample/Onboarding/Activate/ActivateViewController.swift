// ©2021 Mocha. All rights reserved.

import UIKit
import Lottie

final class ActivateViewController: UIViewController {
    @IBOutlet var tapSettingsL: UILabel!
    @IBOutlet var chooseKbdL: UILabel!
    @IBOutlet var allowFullL: UILabel!
    @IBOutlet var toggleKbdIV: UIImageView!
    @IBOutlet var toggleAllowFullIV: UIImageView!
    @IBOutlet var gifIV: AnimationView!
    @IBOutlet weak var allThingsPrivateL: EdgeInsetLabel!
    @IBOutlet weak var activateL: UILabel!
    @IBOutlet weak var tapIV: UIImageView!
    @IBOutlet weak var infoAllowB: UIButton!
    @IBOutlet weak var goToSettingsB: UIButton!
    
    // UI constants for device size adjustaments
    @IBOutlet var logoTC: NSLayoutConstraint!
    @IBOutlet var logoHC: NSLayoutConstraint!

    private var vm: ActivateViewModel!

    func setViewModel(_ vm: ActivateViewModel) {
        self.vm = vm
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        logoTC.constant *= scaleForTopDim
        logoHC.constant *= scaleForLogoHeightDim

        tapSettingsL.attributedText = vm.tapSettingsText
        chooseKbdL.attributedText = vm.chooseKbdText
        allowFullL.attributedText = vm.allowFullText

        setupAnimation()

        updateWithCurrentState()
        
        applyColors()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateWithCurrentState),
            name: UIApplication.willEnterForegroundNotification,
            object: nil
        )
    }

    @objc func updateWithCurrentState() {
        toggleKbdIV.image = vm.toggleKeyboardSwitchImage()
        toggleAllowFullIV.image = vm.toggleAllowFullAccessSwitchImage()

        if vm.everythingIsChecked() {
            OnboardingRouter.showNextOnboardingScreen()
        }
    }

    @IBAction func pressedSettings(_: Any) {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    @IBAction func pressedInfoAllow(_: Any) {
        let vc = BottomDialogPresenter(type: .success).screen()
        OnboardingRouter.showPopup(vc)
    }
    
    // MARK: - UI customization
    
    private func applyColors() {
        view.backgroundColor = vm.backgroundColor
        
        allThingsPrivateL.textColor = vm.allThingsPrivateColor
        activateL.textColor = vm.activateColor
        tapSettingsL.textColor = vm.tapSettingsColor
        chooseKbdL.textColor = vm.chooseKbdColor
        allowFullL.textColor = vm.allowFullColor
        
        tapIV.tintColor = vm.tapIconColor
        infoAllowB.tintColor = vm.infoAllowColor
        
        goToSettingsB.backgroundColor = vm.buttonBackgroundColor
        goToSettingsB.setTitleColor(vm.buttonTitleColor, for: .normal)
    }
    
    private func setupAnimation() {
        gifIV.animation = vm.animationResources.0
        if let imageProvider = vm.animationResources.1 {
            gifIV.imageProvider = imageProvider
        }
        gifIV.loopMode = .loop
        gifIV.play()
    }
}
