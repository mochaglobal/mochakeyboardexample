// ©2021 Mocha. All rights reserved.

import UIKit

final class ActivatePresenter: Presenter<ActivateViewController> {
    init() {
        let vm = ActivateViewModel()
        let vc = ActivateViewController.create()
        vc.setViewModel(vm)
        super.init(vc)
    }
}
