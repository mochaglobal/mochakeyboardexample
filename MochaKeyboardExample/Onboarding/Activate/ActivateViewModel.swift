// ©2021 Mocha. All rights reserved.

import UIKit
import Lottie
import ZIPFoundation
import MochaKeyboardSDK

private let kSwitchOffI = UIImage(named: "ic_toggle_deselected", in: .main, compatibleWith: nil)
private let kSwitchOnI = UIImage(named: "ic_toggle", in: .main, compatibleWith: nil)
private let animationFolder = Bundle.main.resourceURL!.appendingPathComponent("allow_access", isDirectory: true)

final class ActivateViewModel {
    let tapSettingsText = attributedBold(
        for: "In Settings: Tap Keyboards",
        boldText: "Keyboards",
        size: 15.0
    )
    let chooseKbdText = attributedBold(
        for: "Turn on Keemoji",
        boldText: "Keemoji",
        size: 15.0
    )
    let allowFullText = attributedBold(
        for: "Turn on Allow Full Access",
        boldText: "Allow Full Access",
        size: 15.0
    )

    var animationResources: (Animation?, AnimationImageProvider?) {
        var animationDataPath = "data.json"
        if let array = try? FileManager.default.contentsOfDirectory(atPath: animationFolder.relativePath) {
            animationDataPath = array.filter { $0.contains(".json") }.first ?? "data.json"
        }
        return (Animation.filepath(animationFolder.appendingPathComponent(animationDataPath).relativePath),
                FilepathImageProvider(filepath: animationFolder.relativePath))
    }

    init() {
        MochaKeyboardSDK.Permissions.setKeyboardAlreadyChosen(false)
        Defaults.onboardingFinished = false
    }

    func toggleKeyboardSwitchImage() -> UIImage? {
        //return Permissions.isKeyboardAdded() ? kSwitchOnI : kSwitchOffI
        return kSwitchOnI
    }

    func toggleAllowFullAccessSwitchImage() -> UIImage? {
        //return Permissions.isFullAccessAllowed() ? kSwitchOnI : kSwitchOffI
        return kSwitchOnI
    }

    func everythingIsChecked() -> Bool {
        return MochaKeyboardSDK.Permissions.isKeyboardAdded() && MochaKeyboardSDK.Permissions.isFullAccessAllowed()
    }
    
    // MARK: - UI customization

    var backgroundColor: UIColor {
        return OnboardingTheme.background
    }

    var allThingsPrivateColor: UIColor {
        return OnboardingTheme.text2
    }

    var activateColor: UIColor {
        return OnboardingTheme.text2
    }

    var tapSettingsColor: UIColor {
        return OnboardingTheme.text1
    }

    var chooseKbdColor: UIColor {
        return OnboardingTheme.text1
    }

    var allowFullColor: UIColor {
        return OnboardingTheme.text1
    }

    var tapIconColor: UIColor {
        return OnboardingTheme.text1
    }

    var infoAllowColor: UIColor {
        return OnboardingTheme.text1
    }

    var buttonBackgroundColor: UIColor {
        return OnboardingTheme.buttonBackground
    }

    var buttonTitleColor: UIColor {
        return OnboardingTheme.buttonText
    }
}
