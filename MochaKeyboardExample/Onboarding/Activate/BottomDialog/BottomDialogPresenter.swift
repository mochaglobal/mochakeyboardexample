// ©2021 Mocha. All rights reserved.

import UIKit

final class BottomDialogPresenter: Presenter<BottomDialogViewController> {
    init(
        type: DialogType
    ) {
        let vm = BottomDialogViewModel(type: type)
        let vc = BottomDialogViewController.create()
        vc.setViewModel(vm)

        super.init(vc)
    }
}
