// ©2021 Mocha. All rights reserved.

import UIKit

enum DialogType {
    case success
    case error
}

final class BottomDialogViewModel {
    var type: DialogType

    init(type: DialogType) {
        self.type = type
    }
    
    var titleTextColor: UIColor {
        OnboardingTheme.privacyDialogTitleTextColor
    }
    
    var messageTextColor: UIColor {
        OnboardingTheme.privacyDialogMessageTextColor
    }
}
