// ©2021 Mocha. All rights reserved.

import UIKit

final class BottomDialogViewController: UIViewController {
    @IBOutlet var titleL: UILabel!
    @IBOutlet var messageL: UILabel!
    @IBOutlet var iconIV: UIImageView!
    @IBOutlet var containerBC: NSLayoutConstraint!
    @IBOutlet var containerV: UIView!

    private var vm: BottomDialogViewModel!

    func setViewModel(_ vm: BottomDialogViewModel) {
        self.vm = vm
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        containerV.layer.cornerRadius = 32
        containerV.layer.masksToBounds = true
        containerV.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        applyColors()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        containerBC.constant = 0
        UIView.animate(
            withDuration: 0.4,
            delay: 0.2,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: 0.5,
            options: .curveEaseInOut,
            animations: {
                self.view.layoutIfNeeded()
            }
        )
    }

    // MARK: - UI customization
    
    private func applyColors() {
        messageL.textColor = vm.messageTextColor
        titleL.textColor = vm.titleTextColor
    }

    @IBAction func pressedClose(_: Any) {
        dismiss(animated: true)
    }
}
