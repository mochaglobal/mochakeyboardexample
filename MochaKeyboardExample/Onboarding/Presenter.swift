// ©2021 Mocha. All rights reserved.

import UIKit

class Presenter<T: UIViewController> {
    private let vc: T

    init(_ vc: T) {
        self.vc = vc
    }

    func screen() -> T {
        return vc
    }
}
