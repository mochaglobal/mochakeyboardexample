// ©2021 Mocha. All rights reserved.

import UIKit

@objc protocol SelectDelegate: AnyObject {
    @objc optional func didHighlightItemAt(indexPath _: IndexPath)
    @objc optional func didUnhighlightItemAt(indexPath _: IndexPath)
    @objc optional func didSelectItemAt(indexPath _: IndexPath)
}

open class EquallySpacedCollectionView: UICollectionView {
    weak var selectDelegate: SelectDelegate?

    open var direction: UICollectionView.ScrollDirection {
        return .vertical
    }

    open var nrCellsPerLine: Int {
        return 1
    }

    open var spacing: CGFloat {
        return 0
    }

    open var lineSpacing: CGFloat? {
        return nil
    }

    open func insetsForSection(_: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
    }

    private func cellSize(at indexPath: IndexPath?) -> CGSize {
        let size: CGSize = sizeForLayout
        var dim = direction == .vertical ? size.width : size.height
        // OBS: sometimes "cellSize" is called when the view is not ready (ie, "size" is zero); the
        // logic below replaces the view with the entire device screen to avoid crashing
        if dim <= 0.1 {
            dim = direction == .vertical ? Device.width : Device.height
        }
        let nrCellsPerLineF = CGFloat(nrCellsPerLine)
        let s: CGFloat = (dim - (nrCellsPerLineF + 1) * spacing) / nrCellsPerLineF
        if direction == .vertical {
            let h: CGFloat = cellHeightForWidth(s, at: indexPath)
            return CGSize(width: s, height: h)
        } else {
            let w: CGFloat = cellWidthForHeight(s, at: indexPath)
            return CGSize(width: w, height: s)
        }
    }

    open func cellHeightForWidth(_ w: CGFloat, at _: IndexPath?) -> CGFloat {
        return w
    }

    open func cellWidthForHeight(_ h: CGFloat, at _: IndexPath?) -> CGFloat {
        return h
    }

    open var nrSections: Int {
        return 0
    }

    open func nrItems(_: Int) -> Int {
        return 0
    }

    open func cellForIndexPath(_: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }

    /**
     * Returns true if the selection was handled. The flag is used in situations
     * in which the child class needs to know if the parent class already
     * processed the selection.
     */
    open func didHighlightItemAt(indexPath: IndexPath) -> Bool {
        if let f = selectDelegate?.didHighlightItemAt {
            f(indexPath)
            return true
        }
        return false
    }

    /**
     * Returns true if the selection was handled. The flag is used in situations
     * in which the child class needs to know if the parent class already
     * processed the selection.
     */
    open func didUnhighlightItemAt(indexPath: IndexPath) -> Bool {
        if let f = selectDelegate?.didUnhighlightItemAt {
            f(indexPath)
            return true
        }
        return false
    }

    /**
     * Returns true if the selection was handled. The flag is used in situations
     * in which the child class needs to know if the parent class already
     * processed the selection.
     */
    open func didSelectItemAt(indexPath: IndexPath) -> Bool {
        if let f = selectDelegate?.didSelectItemAt {
            f(indexPath)
            return true
        }
        return false
    }

    open func supplementaryView(
        _: String, at _: IndexPath
    ) -> UICollectionReusableView {
        return UICollectionReusableView()
    }

    open func sizeForHeaderInSection(inSection _: Int) -> CGSize {
        return .zero
    }

    open func sizeForFooterInSection(inSection _: Int) -> CGSize {
        return .zero
    }

    public required init(frame: CGRect) {
        let layout = UICollectionViewFlowLayout()
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    open func setup() {
        delegate = self
        dataSource = self
        contentInset = .zero
        contentInsetAdjustmentBehavior = .never

        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = direction
        }
    }

    open var sizeForLayout: CGSize {
        return frame.size
    }

    // MARK: - Helpers

    open func refresh() {
        reloadItems(at: indexPathsForVisibleItems)
    }
}

extension EquallySpacedCollectionView: UICollectionViewDataSource {
    public func numberOfSections(in _: UICollectionView) -> Int {
        return nrSections
    }

    public func collectionView(
        _: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return nrItems(section)
    }

    public func collectionView(
        _: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        return cellForIndexPath(indexPath)
    }

    public func collectionView(
        _: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath
    ) -> UICollectionReusableView {
        return supplementaryView(kind, at: indexPath)
    }
}

extension EquallySpacedCollectionView: UICollectionViewDelegate {
    public func collectionView(
        _: UICollectionView,
        didHighlightItemAt indexPath: IndexPath
    ) {
        _ = didHighlightItemAt(indexPath: indexPath)
    }

    public func collectionView(
        _: UICollectionView,
        didUnhighlightItemAt indexPath: IndexPath
    ) {
        _ = didUnhighlightItemAt(indexPath: indexPath)
    }

    public func collectionView(
        _: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        _ = didSelectItemAt(indexPath: indexPath)
    }
}

extension EquallySpacedCollectionView: UICollectionViewDelegateFlowLayout {
    public func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        return cellSize(at: indexPath)
    }

    public func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        minimumLineSpacingForSectionAt _: Int
    ) -> CGFloat {
        return lineSpacing ?? spacing
    }

    public func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt _: Int
    ) -> CGFloat {
        return spacing
    }

    public func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        insetForSectionAt section: Int
    ) -> UIEdgeInsets {
        return insetsForSection(section)
    }

    public func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        return sizeForHeaderInSection(inSection: section)
    }

    public func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        referenceSizeForFooterInSection section: Int
    ) -> CGSize {
        return sizeForFooterInSection(inSection: section)
    }
}

