// ©2021 Mocha. All rights reserved.

import UIKit

private let kCellId = "ImageCell"
private let kCellHeight: CGFloat = 184

class ImagesCollectionView: EquallySpacedCollectionView {
    var pageChangeCallback: ((Int) -> Void)?

    var imagesUrls: [String] = [] {
        didSet {
            reloadData()
        }
    }

    var margin: CGFloat = 0
    var horizontalSpacing: CGFloat = 0
    unowned var referenceView: UIView!

    override func setup() {
        super.setup()
        register(kCellId)
        isPagingEnabled = true
        showsHorizontalScrollIndicator = false
    }

    override var direction: UICollectionView.ScrollDirection {
        return .horizontal
    }

    override var nrCellsPerLine: Int {
        return 1
    }

    override var spacing: CGFloat {
        return 0
    }

    override var nrSections: Int {
        return 1
    }

    override func nrItems(_: Int) -> Int {
        return imagesUrls.count
    }

    override func cellHeightForWidth(_: CGFloat, at _: IndexPath?) -> CGFloat {
        return kCellHeight
    }

    override func cellWidthForHeight(_: CGFloat, at _: IndexPath?) -> CGFloat {
        return referenceView.frame.width - 2 * margin
    }

    override func cellForIndexPath(_ indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(
            withReuseIdentifier: kCellId,
            for: indexPath
        ) as! ImageCell
        cell.vm = ImageCellViewModel(imagesUrls[indexPath.row])
        cell.horizontalSpacing = horizontalSpacing
        return cell
    }

    // MARK: - UIScrollViewDelegate

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let w = frame.width
        let page = Int((scrollView.contentOffset.x + (0.5 * w)) / w)
        pageChangeCallback?(page)
    }
}
