// ©2021 Mocha. All rights reserved.

import CHIPageControl
import UIKit

private let COLLECTION_MARGIN: CGFloat = 8

class ImagePagerView: BaseNibView {
    @IBOutlet var imagesCV: ImagesCollectionView!
    @IBOutlet var pageControl: CHIPageControlJalapeno!

    var horizontalSpacing: CGFloat = 0 {
        didSet {
            imagesCV.horizontalSpacing = horizontalSpacing
        }
    }

    var picsUrls: [String] = [] {
        didSet {
            imagesCV.imagesUrls = picsUrls
            imagesCV.pageChangeCallback = { [weak self] page in
                if let self = self {
                    self.pageControl.set(progress: page, animated: true)
                    self.currentPage = page
                    self.callback?(page)
                }
            }
            pageControl.numberOfPages = picsUrls.count
            pageControl.set(progress: 0, animated: false)
        }
    }

    var currentPage: Int = 0
    var callback: ((Int) -> Void)?

    func showPage(_ page: Int) {
        imagesCV.scrollToItem(at: IndexPath(row: page, section: 0), at: .left, animated: true)
    }
    
    // MARK: - UI customization
    
    func applyColors(tintColor: UIColor, currentPageTintColor: UIColor) {
        pageControl.tintColor = tintColor
        pageControl.currentPageTintColor = currentPageTintColor
    }
}
