// ©2021 Mocha. All rights reserved.

import SDWebImage
import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet var imageIV: UIImageView!
    @IBOutlet var imageLC: NSLayoutConstraint!
    @IBOutlet var imageTC: NSLayoutConstraint!

    var vm: ImageCellViewModel! {
        didSet {
            if let assetsImage = UIImage(named: vm.img, in: .main, compatibleWith: nil) {
                // image is from Assets
                imageIV.image = assetsImage
            } else {
                // image cannot be loaded
                imageIV.image = nil
            }
        }
    }

    var horizontalSpacing: CGFloat = 0 {
        didSet {
            imageLC.constant = horizontalSpacing
            imageTC.constant = horizontalSpacing
        }
    }
}
