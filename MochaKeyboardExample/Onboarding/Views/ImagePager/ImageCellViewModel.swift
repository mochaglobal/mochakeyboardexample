// ©2021 Mocha. All rights reserved.

import UIKit

class ImageCellViewModel {
    let img: String

    init(_ img: String) {
        self.img = img
    }
}
