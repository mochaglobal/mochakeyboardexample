// ©2021 Mocha. All rights reserved.

import UIKit

class BaseNibView: UIView {
    var rootV: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {
        let nibName = String(describing: type(of: self))
        rootV = Bundle.main.loadNibNamed(
            nibName,
            owner: self,
            options: nil
        )?.first as? UIView
        rootV.translatesAutoresizingMaskIntoConstraints = false
        addSubview(rootV)
        NSLayoutConstraint.activate([
            rootV.bottomAnchor.constraint(equalTo: bottomAnchor),
            rootV.leadingAnchor.constraint(equalTo: leadingAnchor),
            rootV.trailingAnchor.constraint(equalTo: trailingAnchor),
            rootV.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
}
