// ©2021 Mocha. All rights reserved.

import Foundation
import MochaSDK
import MochaKeyboardSDK

extension MochaKeyboardSDKConfig {
    static var config: MochaKeyboardSDKConfig {
        MochaKeyboardSDKConfig(
            rootCampaignId: "9rcjPrqbcU6ieEceADn7Ud",
            keyboardExtensionId: "global.mocha.MochaKeyboardExample.Keyboard",
            backroundTaskIdentifier: "com.keemoji.v2.keyboard.app.update",
            applicationGroupIdentifier: "group.mocha.example.shared",
            appLinkSchema: "mocha-example://",
            logging: .verbose
        )
    }
}

extension MochaPlatformSettings {
    static var settings: MochaPlatformSettings = {
        let buttons: [MochaToolbarButton] = [
            .gifs(),
            .stickers(),
            .settings()
        ]
        var settings = MochaPlatformSettings.default(toolbar: buttons, keyboardLogo: nil)
        return settings
    }()
}
